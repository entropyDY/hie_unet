# -*- coding: utf-8 -*-

"""
Created on 18. 3. 5

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import logging

from lib.image_util import SimpleDataProvider
from lib.unet import Unet, Trainer
from lib.data_util import make_set
from lib.make_patches import equal_divison_image, trainset_random_crop

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")

data_type = "trainset_random_crop"

if __name__ == "__main__":
    if data_type == "equal_division_image":
        pass
    elif data_type == "trainset_random_crop":
        train_path = ["./train/input", "./train/target"]
        valid_path = ["./valid/input", "./valid/target"]

        train_img_names = os.listdir(train_path[0])
        train_mask_names = os.listdir(train_path[1])
        valid_img_names = os.listdir(valid_path[0])
        valid_mask_names = os.listdir(valid_path[1])

        train_imgset = make_set(filenames = train_img_names, path = train_path[0])
        train_maskset = make_set(filenames = train_mask_names, path = train_path[1])
        valid_imgset = make_set(filenames = valid_img_names, path = valid_path[0])
        valid_maskset = make_set(filenames = valid_mask_names, path = valid_path[1])

        train_x, train_y = trainset_random_crop(imgset = train_imgset, maskset = train_maskset, patch_h = 100,
                                                patch_w = 100,
                                                total_pathes = 106000)
        valid_x, valid_y = trainset_random_crop(imgset = valid_imgset, maskset = valid_maskset, patch_h = 100,
                                                patch_w = 100,
                                                total_pathes = 220)

    logging.info(
        "train shape : {}{}, valid_shape : {}{}".format(train_x.shape, train_y.shape, valid_x.shape, valid_y.shape))
    train = SimpleDataProvider(data = train_x, label = train_y, a_min = 0, a_max = 255, channels = train_x.shape[-1],
                               n_class = 2)
    valid = SimpleDataProvider(data = valid_x, label = valid_y, a_min = 0, a_max = 255, channels = valid_x.shape[-1],
                               n_class = 2)

    logging.info("input channels : {}, output class : {}".format(train.channels, train.n_class))

    Hunet = Unet(channels = train.channels, n_class = train.n_class)
    trainer = Trainer(net = Hunet, batch_size = 30)
    trainer.train(data_provider = train, output_path = './trained', decay_times = 1, epochs = 10, dropout = 0.75,
                  display_step = 2, restore = True, write_graph = True, prediciton_path = './prediction',
                  valid_provider = valid)
