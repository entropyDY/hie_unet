# -*- coding: utf-8 -*-

"""
Created on 18. 3. 7

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import logging

import numpy as np

from lib.data_util import make_set
from lib.make_patches import one_test_random_crop, merge_patch, re_process
from lib.image_util import SimpleDataProvider
from lib.unet import Unet
from lib.util import show_image

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")

test_path = ["./test/input", "./test/target"]

test_img_names = os.listdir(test_path[0])
test_mask_names = os.listdir(test_path[1])

test_imgset = make_set(filenames = test_img_names, path = test_path[0])
test_maskset = make_set(filenames = test_mask_names, path = test_path[1])

test = SimpleDataProvider(data = test_imgset, label = test_maskset, a_min = 0, a_max = 255,
                          channels = test_imgset.shape[-1],
                          n_class = test_maskset.shape[-1])

test_x, test_y = test(1)
print(test_y.shape)
show_image(re_process(test_x[0]))
show_image(test_y[0])
one_test_x, _, merge_size = one_test_random_crop(img = test_x, mask = test_y, patch_h = 100, patch_w = 100)

Hunet = Unet(channels = test.channels, n_class = 2)

prediction = Hunet.predict("./trained/model-7", one_test_x)

prediction = re_process(prediction)

prediction = merge_patch(prediction)
show_image(prediction[...,0])
