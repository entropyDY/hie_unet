# -*- coding: utf-8 -*-

"""
Created on 18. 3. 8

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os

from lib.util import load_image, show_image
from lib.data_util import make_set
from lib.make_patches import equal_divison_image, trainset_random_crop, one_test_random_crop, merge_patch, re_process
from lib.image_util import SimpleDataProvider

"""
img_path = "./train/input"
mask_path = "./train/target"
img_names = os.listdir(img_path)
mask_names = os.listdir(mask_path)
imgset = make_set(filenames = img_names, path = img_path)
maskset = make_set(filenames = mask_names, path = mask_path)
print(imgset.shape)
print(maskset.shape)
train_x, train_y = trainset_random_crop(imgset = imgset, maskset = maskset, patch_h = 256, patch_w = 256, total_pathes = 106)

show_image(train_x[0])
show_image(train_y[0])
"""
img_path = "./test/input"
mask_path = "./test/target"

img_names = os.listdir(img_path)
mask_names = os.listdir(mask_path)
imgset = make_set(filenames = img_names, path = img_path)
maskset = make_set(filenames = mask_names, path = mask_path)

test = SimpleDataProvider(data = imgset, label = maskset, a_min = 0, a_max = 255, n_class = 2)
test_x, test_y = test(1)

one_test_x, one_test_y, _ = one_test_random_crop(test_x, test_y, patch_h = 100, patch_w = 100)

merge = merge_patch(one_test_x, 500, 500)

one_test_x = re_process(one_test_x[0])
test_x = re_process(test_x[0])
merge = re_process(merge)

show_image(np_img = one_test_x, title = "crop_img")
show_image(np_img = test_x, title = "original img")
show_image(np_img = merge, title = "merge img")

