# -*- coding: utf-8 -*-

"""
Created on 18. 3. 7

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import logging
import shutil

import numpy as np

from lib.make_patches import train_random_crop

original_data_path = "./original_data"

operator_sw = {"original split": False, "train random crop": True}

N_pathes_train = 212000
N_pathes_valid = 4400
patch_h = 48
patch_w = 48


"""
if operator_sw["original split"]:
    split_data(data_path = original_data_path, rate = [9, 1, 1])

elif operator_sw["train random crop"]:
    train_path = "./train"
    valid_path = "./valid"

    train_names = os.listdir("{path}/input".format(path = train_path))
    valid_names = os.listdir("{path}/input".format(path = valid_path))

    train_x, train_y = split_x_y(train_names, train_path)
    valid_x, valid_y = split_x_y(valid_names, valid_path)

    save_image(train_x[0], path = "{save_path}/{filename}_input.png".format(save_path = ".", filename = "temp"))

    logging.info("\ntrain shape : {}, {}\nvalid shape : {}, {}".format(train_x.shape, train_y.shape, valid_x.shape,
                                                                       valid_y.shape))

    train_x, train_y = train_random_crop(train_x, train_y, patch_h = patch_h, patch_w = patch_w, N_patches = N_pathes_train)
    valid_x, valid_y = train_random_crop(valid_x, valid_y, patch_h = patch_h, patch_w = patch_w, N_patches = N_pathes_valid)

    logging.info("\ntrain shape : {}, {}\nvalid shape : {}, {}".format(train_x.shape, train_y.shape, valid_x.shape,
                                                                       valid_y.shape))

    crop_train_path = "./crop_train"
    crop_valid_path = "./crop_valid"

    for path in [crop_train_path, crop_valid_path]:
        if os.path.exists(os.path.abspath(path)):
            logging.info("Removing '{:}'".format(path))
            shutil.rmtree(os.path.abspath(path), ignore_errors = True)
        logging.info("Allocating '{:}'".format(path))
        os.makedirs(os.path.abspath(path))

    for i in np.random.randint(low = 0, high = N_pathes_train, size = 10):
        save_image(train_x[i], path = "{save_path}/{filename}_input.png".format(save_path = crop_train_path, filename = i))

"""