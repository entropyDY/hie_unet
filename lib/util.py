# -*- coding: utf-8 -*-

"""
Created on 18. 2. 28

@author : doyoung
@project name : Unet_HIE
@description
 - 데이터 입력, 저장
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import tensorflow as tf
import numpy as np

from PIL import Image


def gpu_config():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return config


def to_rgb(img):
    img = np.atleast_3d(img)
    channels = img.shape[2]
    if channels < 3:
        img = np.tile(img, 3)

    img[np.isnan(img)] = 0
    img -= np.amin(img)
    img /= np.amax(img)
    img *= 255
    return img


def crop_to_shape(data, shape):
    offset0 = (data.shape[1] - shape[1]) // 2
    offset1 = (data.shape[2] - shape[2]) // 2
    return data[:, offset0:(-offset0), offset1:(-offset1)]


def combine_img_prediction(data, gt, pred):
    ny = pred.shape[2]
    ch = data.shape[3]
    img = np.concatenate((to_rgb(crop_to_shape(data, pred.shape).reshape(-1, ny, ch)),
                          to_rgb(crop_to_shape(gt[..., 0], pred.shape).reshape(-1, ny, 1)),
                          to_rgb(pred[..., 0].reshape(-1, ny, 1))), axis = 1)
    return img


def load_image(path, dtype = np.float32):
    return np.array(Image.open(path).convert('L'), dtype)


def save_image(np_img, path):
    Image.fromarray(np_img.round().astype(np.uint8)).save(path, 'JPEG', dpi = [300, 300], quality = 90)


def show_image(np_img, title = "", color_space = 'L'):
    img_channel = 1 if len(np_img.shape) == 2 else np_img.shape[-1]
    if img_channel == 1:
        np_img = np_img.reshape((np_img.shape[0], np_img.shape[1]))
    else:
        np_img = np_img.reshape((np_img.shape[0], np_img.shape[1], np_img.shape[2]))
    img = Image.fromarray(np_img.astype(np.uint8), color_space)
    img.show(title = title)
