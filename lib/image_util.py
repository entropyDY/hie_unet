# -*- coding: utf-8 -*-

"""
Created on 18. 3. 5

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import glob

import numpy as np

from PIL import Image


class BasicDataProvider(object):
    channels = 3
    n_class = 2

    def __init__(self, a_min = None, a_max = None):
        self.a_min = a_min if a_min is not None else -np.inf
        self.a_max = a_max if a_max is not None else np.inf

    def _load_data_and_label(self):
        data, label = self._next_data()

        train_data = self._process_data(data)
        labels = self._process_labels(label)

        train_data, labels = self._post_process(train_data, labels)

        nx = train_data.shape[1]
        ny = train_data.shape[0]

        return train_data.reshape(1, ny, nx, self.channels), labels.reshape(1, ny, nx, self.n_class)

    def _process_labels(self, label):
        if self.n_class == 2:
            nx = label.shape[1]
            ny = label.shape[0]
            label = np.reshape(label, (label.shape[0], label.shape[1]))if len(label.shape) > 2 else label
            label = label.astype(np.bool)
            labels = np.zeros((ny, nx, self.n_class), dtype = np.float32)
            labels[..., 1] = label
            labels[..., 0] = ~label
            return labels

        return label

    def _process_data(self, data):
        data = np.clip(np.fabs(data), self.a_min, self.a_max)
        data -= np.amin(data)
        data /= np.amax(data)
        return data

    def _post_process(self, data, labels):
        return data, labels

    def __call__(self, n):
        train_data, labels = self._load_data_and_label()
        nx = train_data.shape[1]
        ny = train_data.shape[2]

        X = np.zeros((n, nx, ny, self.channels))
        Y = np.zeros((n, nx, ny, self.n_class))

        X[0] = train_data
        Y[0] = labels

        for i in range(1, n):
            train_data, labels = self._load_data_and_label()
            X[i] = train_data
            Y[i] = labels

        return X, Y


class SimpleDataProvider(BasicDataProvider):
    def __init__(self, data, label, a_min = None, a_max = None, channels = 1, n_class = 1):
        super(SimpleDataProvider, self).__init__(a_min, a_max)
        self.data = data
        self.label = label
        self.file_count = data.shape[0]
        self.n_class = n_class
        self.channels = channels

    def _next_data(self):
        idx = np.random.choice(self.file_count)
        return self.data[idx], self.label[idx]


class ImageDataProvider(BasicDataProvider):
    def __init__(self, search_path, a_min = None, a_max = None, data_suffix = ".png", mask_suffix = ".png",
                 shuffle_data = True, n_class = 2):
        super(ImageDataProvider, self).__init__(a_min, a_max)
        self.data_shuffix = data_suffix
        self.mask_shuffix = mask_suffix
        self.file_idx = -1
        self.shuffle_data = shuffle_data
        self.n_class = n_class

        self.data_files = self._find_data_files(search_path)

        if self.shuffle_data:
            np.random.shuffle(self.data_files)

        assert len(self.data_files) > 0, "No training files"
        print("Number of files used : %s" % len(self.data_files))

        img = self._load_file(self.data_files[0])
        self.channels = 1 if len(img.shape) == 2 else img.shape[-1]

    def _find_data_files(self, search_path):
        all_files = glob.glob(search_path)
        return [name for name in all_files if self.data_shuffix in name and not self.mask_shuffix in name]

    def _load_file(self, path, dtype = np.float32):
        return np.array(Image.open(path).convert('L'), dtype)

    def _cycle_file(self):
        self.file_idx += 1
        if self.file_idx >= len(self.data_files):
            self.file_idx = 0
            if self.shuffle_data:
                np.random.shuffle(self.data_files)

    def _next_data(self):
        self._cycle_file()
        image_name = self.data_files[self.file_idx]
        label_name = image_name.replace(self.data_shuffix, self.mask_shuffix)

        img = self._load_file(image_name, np.float32)
        label = self._load_file(label_name, np.bool)

        return img, label
