# -*- coding: utf-8 -*-

"""
Created on 18. 2. 28

@author : doyoung
@project name : Unet_HIE
@description
 - layers는 deep learning 에서 사용되는 layer를 구현해 놓음.

 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import sys
import logging

import tensorflow as tf

logging.basicConfig(level = logging.DEBUG, format = "%(asctime)s %(message)s")


def _xavier_weight(name, shape, uniform = True, seed = None, dtype = tf.float32):
    init = tf.contrib.layers.xavier_initializer(uniform = uniform, seed = seed, dtype = dtype)
    return tf.get_variable(name = name, shape = shape, dtype = dtype, initializer = init)


def _random_normal_weight(name, shape, stddev = 0.1, seed = None, dtype = tf.float32):
    init = tf.random_normal_initializer(stddev = stddev, seed = seed, dtype = dtype)
    return tf.get_variable(name = name, shape = shape, dtype = dtype, initializer = init)


def _truncated_normal_weight(name, shape, stddev = 0.1, seed = None, dtype = tf.float32):
    init = tf.truncated_normal_initializer(stddev = stddev, seed = seed, dtype = dtype)
    return tf.get_variable(name = name, shape = shape, dtype = dtype, initializer = init)


def _random_uniform_weight(name, shape, minval = 0, maxval = None, seed = None, dtype = tf.float32):
    init = tf.random_uniform_initializer(minval = minval, maxval = maxval, seed = seed, dtype = dtype)
    return tf.get_variable(name = name, shape = shape, dtype = dtype, initializer = init)


def _constant_bias(name, shape, value = 0, dtype = tf.float32):
    init = tf.constant_initializer(value = value, dtype = dtype)
    return tf.get_variable(name = name, shape = shape, dtype = dtype, initializer = init)


def select_weight_bias(name, shape, select = 'XAVIER', opt_kwargs = {}):
    """
    weight & bias 선택
    :param select: weight & bias 의 초기 형태 선택
    :param opt_kwargs: weight & bias 설정 파라미터 입력
    :return: weight & bias
    """
    uniform = opt_kwargs.pop('uniform', True)  # xavier parameter
    stddev = opt_kwargs.pop('stddev', 0.1)  # truncated and random normal parameter
    seed = opt_kwargs.pop('seed', None)  # weight parameter
    value = opt_kwargs.pop('value', 0.0)  # bias parameter
    minval = opt_kwargs.pop('minval', 0.0)  # random uniform parameter
    maxval = opt_kwargs.pop('maxval', None)  # random uniform parameter
    dtype = opt_kwargs.pop('dtype', tf.float32)  # all parameter

    if select == 'XAVIER':
        return _xavier_weight(name = name, shape = shape, uniform = uniform, seed = seed, dtype = dtype)

    elif select == 'NORMAL':
        return _random_normal_weight(name = name, shape = shape, stddev = stddev, seed = seed, dtype = dtype)

    elif select == 'TRUNCATED':
        return _truncated_normal_weight(name = name, shape = shape, stddev = stddev, seed = seed, dtype = dtype)

    elif select == 'UNIFORM':
        return _random_uniform_weight(name = name, shape = shape, minval = minval, maxval = maxval, seed = seed,
                                      dtype = dtype)
    elif select == 'BIAS':
        return _constant_bias(name = name, shape = shape, value = value, dtype = dtype)

    else:
        logging.warning('weight_bias_select is not correct')
        sys.exit(1)


def conv2d(x, W, keep_prob_):
    conv_2d = tf.nn.conv2d(x, W, strides = [1, 1, 1, 1], padding = 'VALID')
    return tf.nn.dropout(conv_2d, keep_prob_)


def max_pool(x, n):
    return tf.nn.max_pool(x, ksize = [1, n, n, 1], strides = [1, n, n, 1], padding = 'VALID')


def deconv2d(x, W, stride):
    x_shape = tf.shape(x)
    output_shape = tf.stack([x_shape[0], x_shape[1] * 2, x_shape[2] * 2, x_shape[3] // 2])
    return tf.nn.conv2d_transpose(x, W, output_shape, strides = [1, stride, stride, 1], padding = 'VALID')


def crop_and_concat(x1, x2):
    x1_shape = tf.shape(x1)
    x2_shape = tf.shape(x2)
    # offsets for the top left corner of the crop
    offsets = [0, (x1_shape[1] - x2_shape[1]) // 2, (x1_shape[2] - x2_shape[2]) // 2, 0]
    size = [-1, x2_shape[1], x2_shape[2], -1]
    x1_crop = tf.slice(x1, offsets, size)
    return tf.concat([x1_crop, x2], 3)


def pixel_wise_softmax(output_map):
    exponential_map = tf.exp(output_map)
    evidence = tf.add(exponential_map, tf.reverse(exponential_map, [False, False, False, True]))
    return tf.div(exponential_map, evidence, name = "pixel_wise_softmax")


def pixel_wise_softmax_2(output_map):
    exponential_map = tf.exp(output_map)
    sum_exp = tf.reduce_sum(exponential_map, 3, keep_dims = True)
    tensor_sum_exp = tf.tile(sum_exp, tf.stack([1, 1, 1, tf.shape(output_map)[3]]))
    return tf.div(exponential_map, tensor_sum_exp)


def cross_entropy(y_, output_map):
    return -tf.reduce_mean(y_ * tf.log(tf.clip_by_value(output_map, 1e-10, 1.0)), name = "cross_entropy")
