# -*- coding: utf-8 -*-

"""
Created on 18. 3. 7

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

import numpy as np

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")


def equal_divison_image(img, n_div_h, n_div_w):
    img_h, img_w, _ = img.shape
    h_step = img_h if n_div_h == 0 else img_h // n_div_h
    w_step = img_w if n_div_w == 0 else img_w // n_div_w

    crop_imgset = np.zeros((n_div_h * n_div_w, h_step, w_step, img.shape[-1]))

    for wp in range(0, img_w, w_step):
        for hp, i in zip(range(0, img_h, h_step), range(n_div_h * n_div_w)):
            crop_img = img[hp:hp + h_step, wp:wp + w_step]
            crop_imgset[i] = crop_img

    return crop_imgset


def trainset_random_crop(imgset, maskset, patch_h, patch_w, total_pathes = 212000):
    assert not (total_pathes % imgset.shape[0] != 0), "total_pathes : please enter a multiple of {} images".format(
        imgset.shape[0])
    patch_per_img = int(total_pathes / imgset.shape[0])
    img_h, img_w = imgset.shape[1], imgset.shape[2]
    dw = int(patch_w / 2)
    dh = int(patch_h / 2)
    patches = np.empty((total_pathes, patch_h, patch_w, imgset.shape[-1]))
    patches_masks = np.empty((total_pathes, patch_h, patch_w, maskset.shape[-1]))
    iter_total = 0
    for i in range(imgset.shape[0]):
        for k in range(patch_per_img):
            x_center = np.random.randint(0 + dw, img_w - dw)
            y_center = np.random.randint(0 + dh, img_h - dh)

            patch = imgset[i, y_center - dh: y_center + dh, x_center - dw: x_center + dw, :]
            patch_mask = maskset[i, y_center - dh: y_center + dh, x_center - dw: x_center + dw, :]

            patches[iter_total] = patch
            patches_masks[iter_total] = patch_mask

            iter_total += 1

    return patches, patches_masks


def one_test_random_crop(img, mask, patch_h, patch_w):
    img_h, img_w = img.shape[1], img.shape[2]

    h_size = (img_h - (img_h % patch_h)) if img_h % patch_h != 0 else img_h
    w_size = (img_w - (img_w % patch_w)) if img_w % patch_w != 0 else img_w

    total_patches = int((h_size / patch_h) * (w_size / patch_w))

    logging.info("image shape : {}, h_size : {}, w_size : {}, total patches : {}".format(img.shape, h_size, w_size,
                                                                                         total_patches))
    merge_size = [h_size, w_size]

    patches = np.zeros((total_patches, patch_h, patch_w, img.shape[-1]))
    patches_masks = np.zeros((total_patches, patch_h, patch_w, mask.shape[-1]))

    i = 0
    for hp in range(0, h_size, patch_h):
        for wp in range(0, w_size, patch_w):
            patch = img[-1, hp:hp + patch_h, wp:wp + patch_w, :]
            patch_mask = mask[-1, hp:hp + patch_h, wp:wp + patch_w, :]

            patches[i] = patch
            patches_masks[i] = patch_mask
            i += 1

    return patches, patches_masks, merge_size


def merge_patch(patches):
    n_patch = patches.shape[0]
    step_h = patches.shape[1]
    step_w = patches.shape[2]
    channel = patches.shape[-1]
    print(n_patch, step_w, step_h, channel)
    merge_size = int(np.sqrt(n_patch))
    merge = np.zeros((merge_size * step_h, merge_size * step_w, channel))

    logging.info("patch shape : {}".format(patches.shape))
    logging.info("merge shape : {}".format(merge.shape))

    i = 0
    for hp in range(0, merge_size * step_h, step_h):
        for wp in range(0, merge_size * step_w, step_w):
            merge[hp:hp + step_h, wp: wp + step_w, ...] = patches[i]
            i += 1
    return merge


def re_process(img, a_min = 0, a_max = 255):
    img *= a_max
    img += a_min

    return img
