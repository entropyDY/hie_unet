# -*- coding: utf-8 -*-

"""
Created on 18. 3. 4

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from collections import OrderedDict

import tensorflow as tf

from lib.layers import select_weight_bias, conv2d, max_pool, deconv2d, crop_and_concat

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")

def get_image_summary(img, idx = 0):
    """
    Make an image summary for 4d tensor image with index idx
    """

    V = tf.slice(img, (0, 0, 0, idx), (1, -1, -1, 1))
    V -= tf.reduce_min(V)
    V /= tf.reduce_max(V)
    V *= 255

    img_w = tf.shape(img)[1]
    img_h = tf.shape(img)[2]
    V = tf.reshape(V, tf.stack((img_w, img_h, 1)))
    V = tf.transpose(V, (2, 0, 1))
    V = tf.reshape(V, tf.stack((-1, img_w, img_h, 1)))
    return V


def create_unet(x, keep_prob, channels, n_class, layers = 3, features_root = 16, filter_size = 3, pool_size = 2,
                summaries = True):
    logging.info(
        "Layers {layers}, features {features}, filter size {filter_size}x{filter_size}, pool size: {pool_size}x{pool_size}".format(
            layers = layers,
            features = features_root,
            filter_size = filter_size,
            pool_size = pool_size))
    nx = tf.shape(x)[1]
    ny = tf.shape(x)[2]
    x_image = tf.reshape(x, tf.stack([-1, nx, ny, channels]))
    in_node = x_image
    batch_size = tf.shape(x_image)[0]

    weights = []
    biases = []
    convs = []
    pools = OrderedDict()
    deconv = OrderedDict()
    dw_h_convs = OrderedDict()
    up_h_convs = OrderedDict()

    in_size = 1024
    size = in_size
    for layer in range(0, layers):
        features = 2 ** layer * features_root
        with tf.variable_scope(name_or_scope = "dw_conv{layer}".format(layer = layer)):
            if layer == 0:
                w1 = select_weight_bias(name = "w1", shape = [filter_size, filter_size, channels, features],
                                        select = 'XAVIER')
            else:
                w1 = select_weight_bias(name = "w1", shape = [filter_size, filter_size, features // 2, features],
                                        select = 'XAVIER')
            w2 = select_weight_bias(name = "w2", shape = [filter_size, filter_size, features, features],
                                    select = 'XAVIER')
            b1 = select_weight_bias(name = "b1", shape = [features], select = 'BIAS')
            b2 = select_weight_bias(name = "b2", shape = [features], select = 'BIAS')

            conv1 = conv2d(in_node, w1, keep_prob)
            tmp_h_conv = tf.nn.relu(tf.add(conv1, b1))
            conv2 = conv2d(tmp_h_conv, w2, keep_prob)
            dw_h_convs[layer] = tf.nn.relu(tf.add(conv2, b2))

            weights.append((w1, w2))
            biases.append((b1, b2))
            convs.append((conv1, conv2))

        size -= 4
        if layer < layers-1:
            with tf.variable_scope(name_or_scope = "dw_pool{layer}".format(layer = layer)):
                pools[layer] = max_pool(dw_h_convs[layer], pool_size)
                in_node = pools[layer]
                size /= 2

    in_node = dw_h_convs[layers - 1]

    for layer in range(layers - 2, -1, -1):
        features = 2 ** (layer + 1) * features_root
        with tf.variable_scope("up_deconv{layer}".format(layer = layer)):
            wd = select_weight_bias(name = "wd", shape = [pool_size, pool_size, features // 2, features],
                                    select = 'XAVIER')
            bd = select_weight_bias(name = "bd", shape = [features // 2], select = 'BIAS')
            h_deconv = tf.nn.relu(tf.add(deconv2d(in_node, wd, pool_size), bd))
            h_deconv_concat = crop_and_concat(dw_h_convs[layer], h_deconv)
            deconv[layer] = h_deconv_concat

        with tf.variable_scope("up_layer{layer}".format(layer = layer)):
            w1 = select_weight_bias(name = "w1", shape = [filter_size, filter_size, features, features // 2],
                                    select = 'XAVIER')
            w2 = select_weight_bias(name = "w2", shape = [filter_size, filter_size, features // 2, features // 2],
                                    select = 'XAVIER')
            b1 = select_weight_bias(name = "b1", shape = [features // 2], select = 'BIAS')
            b2 = select_weight_bias(name = "b2", shape = [features // 2], select = 'BIAS')

            conv1 = conv2d(h_deconv_concat, w1, keep_prob)
            h_conv = tf.nn.relu(tf.add(conv1, b1))
            conv2 = conv2d(h_conv, w2, keep_prob)
            in_node = tf.nn.relu(tf.add(conv2, b2))
            up_h_convs[layer] = in_node

            weights.append((w1, w2))
            biases.append((b1, b2))
            convs.append((conv1, conv2))

        size *= 2
        size -= 4

    with tf.variable_scope("output"):
        w = select_weight_bias(name = "w", shape = [1, 1, features_root, n_class],
                               select = 'XAVIER')
        b = select_weight_bias(name = "b", shape = [n_class], select = 'BIAS')
        conv = conv2d(in_node, w, tf.constant(1.0))
        output_map = tf.nn.relu(tf.add(conv, b))
        up_h_convs["out"] = output_map

    if summaries:
        for i, (c1, c2) in enumerate(convs):
            tf.summary.image('summary_conv_%02d_01' % i, get_image_summary(c1))
            tf.summary.image('summary_conv_%02d_02' % i, get_image_summary(c2))

        for k in pools.keys():
            tf.summary.image('summary_pool_%02d' % k, get_image_summary(pools[k]))

        for k in deconv.keys():
            tf.summary.image('summary_deconv_concat_%02d' % k, get_image_summary(deconv[k]))

        for k in dw_h_convs.keys():
            tf.summary.histogram("dw_convolution_%02d" % k + '/activations', dw_h_convs[k])

        for k in up_h_convs.keys():
            tf.summary.histogram("up_convolution_%s" % k + '/activations', up_h_convs[k])

    variables = []
    for w1, w2 in weights:
        variables.append(w1)
        variables.append(w2)

    for b1, b2 in biases:
        variables.append(b1)
        variables.append(b2)

    return output_map, variables, int(in_size - size)



