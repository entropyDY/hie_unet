# -*- coding: utf-8 -*-

"""
Created on 18. 3. 8

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import shutil
import logging

import numpy as np

from lib.util import load_image

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")


def split_data(data_path, rate = [8, 1, 1], paths = ["./train", "./valid", "./test"]):
    """
    원본 데이터가 있는 폴더의 데이터를 학습용, 평가용, 확인용으로 분리 후 저장
    data를 train, valid, test 로 분리하는 함수
    :param data_path: 원본 데이터가 존재하는 경로
    :param rate: train, valid, test 비율
    :return:
    """
    assert not len(rate) == 3, "rate length must be 3"
    assert not len(paths) == 3, "paths length must be 3"

    make_directory(paths)

    filenames = list(set(int(filename.split(".")[0].split("_")[0]) for filename in os.listdir(data_path)))
    np.random.shuffle(filenames)

    total_rate = rate[0] + rate[1] + rate[2]
    rate = [rate[0] / total_rate, rate[1] / total_rate, rate[2] / total_rate]

    total_size = len(filenames)
    train_size = int(total_size * rate[0])
    valid_size = int(total_size * rate[1])
    test_size = int(total_size * rate[2])

    valid_files = filenames[:valid_size]
    test_files = filenames[valid_size:valid_size + test_size]
    train_files = filenames[valid_size + test_size:]

    logging.info("total : {}, train : {}, valid : {}, test : {}".format(total_size, train_size, valid_size, test_size))

    for files, fpath in zip([train_files, valid_files, test_files], paths):
        for file in files:
            shutil.copyfile("{ori}/{filename}.png".format(ori = data_path, filename = file),
                            "{fpath}/input/{filename}.png".format(fpath = fpath, filename = file))
            shutil.copyfile("{ori}/{filename}_mask.png".format(ori = data_path, filename = file),
                            "{fpath}/target/{filename}.png".format(fpath = fpath, filename = file))

            shutil.copyfile("{ori}/{filename}.png".format(ori = data_path, filename = file),
                            "{fpath}/total/{filename}.png".format(fpath = fpath, filename = file))
            shutil.copyfile("{ori}/{filename}_mask.png".format(ori = data_path, filename = file),
                            "{fpath}/total/{filename}_mask.png".format(fpath = fpath, filename = file))


def make_set(filenames, path):
    begin = load_image(path = "{path}/{filename}".format(path = path, filename = filenames[0]))
    n_channel = 1 if len(begin.shape) == 2 else begin.shape[-1]
    temp_set = np.zeros(shape = [len(filenames), begin.shape[0], begin.shape[1], n_channel])

    for i, filename in zip(range(len(filenames)), filenames):
        img = load_image(path = "{path}/{filename}".format(path = path, filename = filename)).reshape(
            (begin.shape[0], begin.shape[1], n_channel))
        temp_set[i] = img

    return temp_set


def make_directory(paths):
    for path in paths:
        if os.path.exists(os.path.abspath(path)):
            logging.info("Removing '{:}'".format(path))
            shutil.rmtree(os.path.abspath(path), ignore_errors = True)
        logging.info("Allocating '{:}'".format(path))
        os.makedirs(os.path.abspath(path))

    for path in paths:
        logging.info("Allocating '{path}/input'".format(path = path))
        logging.info("Allocating '{path}/target'".format(path = path))
        logging.info("Allocating '{path}/total'".format(path = path))
        os.makedirs("{path}/input".format(path = os.path.abspath(path)))
        os.makedirs("{path}/target".format(path = os.path.abspath(path)))
        os.makedirs("{path}/total".format(path = os.path.abspath(path)))
