# -*- coding: utf-8 -*-

"""
Created on 18. 3. 1

@author : doyoung
@project name : Unet_HIE
@description
 - 
 
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import logging
import shutil

import tensorflow as tf
import numpy as np

import lib.util as util

from lib.network import create_unet
from lib.layers import cross_entropy, pixel_wise_softmax_2

logging.basicConfig(level = logging.INFO, format = "%(asctime)s %(message)s")


class Unet(object):
    def __init__(self, channels = 3, n_class = 2, cost = "cross_entropy", cost_kwargs = {}, **kwargs):
        tf.reset_default_graph()

        self.n_class = n_class
        self.summaries = kwargs.get("summaries", True)

        self.x = tf.placeholder(dtype = tf.float32, shape = [None, None, None, channels])
        self.y = tf.placeholder(dtype = tf.float32, shape = [None, None, None, n_class])
        self.keep_prob = tf.placeholder(dtype = tf.float32)

        logits, self.variables, self.offset = create_unet(x = self.x, keep_prob = self.keep_prob, channels = channels,
                                                          n_class = n_class, **kwargs)

        self.cost = self._get_cost(logits, cost, cost_kwargs)

        self.gradients_node = tf.gradients(self.cost, self.variables)

        self.cross_entropy = tf.reduce_mean(
            cross_entropy(tf.reshape(self.y, [-1, n_class]), tf.reshape(pixel_wise_softmax_2(logits), [-1, n_class])))

        self.predicter = pixel_wise_softmax_2(logits)
        self.correct_pred = tf.equal(tf.argmax(self.predicter, 3), tf.argmax(self.y, 3))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

    def _get_cost(self, logits, cost_name, cost_kwargs):
        flat_logits = tf.reshape(logits, [-1, self.n_class])
        flat_labels = tf.reshape(self.y, [-1, self.n_class])

        if cost_name == "cross_entropy":
            logging.info("cost function is cross entropy")
            class_weights = cost_kwargs.pop("class_weights", None)

            if class_weights is not None:
                class_weights = tf.constant(np.array(class_weights, dtype = np.float32))

                weight_map = tf.multiply(flat_labels, class_weights)
                weight_map = tf.reduce_sum(weight_map, axis = 1)

                loss_map = tf.nn.softmax_cross_entropy_with_logits(logits = flat_logits, labels = flat_labels)

                weighted_loss = tf.multiply(loss_map, weight_map)

                loss = tf.reduce_mean(weighted_loss)

            else:
                loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits(logits = flat_logits, labels = flat_labels))
        elif cost_name == "dice_coefficient":
            logging.info("cost function is dice coefficient")
            eps = 1e-5
            prediction = pixel_wise_softmax_2(logits)
            intersection = tf.reduce_sum(prediction * self.y)
            union = eps + tf.reduce_sum(prediction) + tf.reduce_sum(self.y)
            loss = -(2 * intersection / (union))

        else:
            raise ValueError("Unknwon cost function : {}".format(cost_name))

        regularizer = cost_kwargs.pop("regularizer", None)
        if regularizer is not None:
            regularizers = sum([tf.nn.l2_loss(variable) for variable in self.variables])
            loss += (regularizer * regularizers)

        return loss

    def predict(self, model_path, x_test):
        init = tf.global_variables_initializer()
        with tf.Session(config = util.gpu_config()) as sess:
            sess.run(init)
            self.restore(sess, model_path)

            y_dummy = np.empty((x_test.shape[0], x_test.shape[1], x_test.shape[2], self.n_class))
            prediction = sess.run(self.predicter, feed_dict = {self.x: x_test, self.y: y_dummy, self.keep_prob: 1.})

        return prediction

    def save(self, sess, model_path, global_step):
        saver = tf.train.Saver()
        save_path = saver.save(sess, model_path, global_step = global_step)
        logging.info("weight save in {}".format(save_path))

    def restore(self, sess, model_path):
        saver = tf.train.Saver()
        saver.restore(sess, model_path)
        logging.info("Model restored from file %s" % model_path)


class Trainer(object):
    verification_batch_size = 4

    def __init__(self, net, batch_size = 1, norm_grads = False, optimizer = "momentum", opt_kwargs = {}):
        self.net = net
        self.batch_size = batch_size
        self.norm_grads = norm_grads
        self.optimizer = optimizer
        self.opt_kwargs = opt_kwargs

    def _get_optimizer(self, decay_steps):
        learning_rate = self.opt_kwargs.pop("learning_rate", 0.2)
        decay_rate = self.opt_kwargs.pop("decay_rate", 0.95)
        self.learning_rate_node = tf.train.exponential_decay(learning_rate = learning_rate,
                                                             global_step = self.global_step, decay_steps = decay_steps,
                                                             decay_rate = decay_rate, staircase = True)
        if self.optimizer == "momentum":
            momentum = self.opt_kwargs.pop("momentum", 0.2)
            optimizer = tf.train.MomentumOptimizer(learning_rate = self.learning_rate_node, momentum = momentum,
                                                   **self.opt_kwargs).minimize(self.net.cost,
                                                                               global_step = self.global_step)
        elif self.optimizer == "adam":
            optimizer = tf.train.AdamOptimizer(learning_rate = self.learning_rate_node, **self.opt_kwargs).minimize(
                self.net.cost, global_step = self.global_step)

        return optimizer

    def _initialize(self, decay_steps, output_path, restore, prediction_path):
        self.global_step = tf.Variable(0)
        self.norm_gradients_node = tf.Variable(tf.constant(0.0, shape = [len(self.net.gradients_node)]))

        if self.net.summaries and self.norm_grads:
            tf.summary.histogram("norm_grads", self.norm_gradients_node)

        tf.summary.scalar("loss", self.net.cost)
        tf.summary.scalar("cross_entropy", self.net.cross_entropy)
        tf.summary.scalar("accuracy", self.net.accuracy)

        self.optimizer = self._get_optimizer(decay_steps = decay_steps)
        tf.summary.scalar("learning_rate", self.learning_rate_node)

        self.summary_op = tf.summary.merge_all()
        init = tf.global_variables_initializer()

        self.prediction_path = prediction_path
        abs_prediction_path = os.path.abspath(prediction_path)
        output_path = os.path.abspath(output_path)

        if not restore:
            logging.info("Removing '{:}'".format(abs_prediction_path))
            shutil.rmtree(abs_prediction_path, ignore_errors = True)
            logging.info("Removing '{:}'".format(output_path))
            shutil.rmtree(output_path, ignore_errors = True)

        if not os.path.exists(abs_prediction_path):
            logging.info("Allocating '{:}'".format(abs_prediction_path))
            os.makedirs(abs_prediction_path)

        if not os.path.exists(output_path):
            logging.info("Allocating '{:}'".format(output_path))
            os.makedirs(output_path)

        return init

    def train(self, data_provider, output_path, decay_times = 1, epochs = 10, dropout = 0.75, display_step = 1,
              restore = False, write_graph = False, prediciton_path = "prediction", remove_save = False,
              valid_provider = None):

        epoch_iters = data_provider.file_count // self.batch_size

        save_path = os.path.join(output_path, "model")
        if not remove_save:
            logging.info("Removing '{:}'".format(output_path))
            shutil.rmtree(output_path, ignore_errors = True)
        if epochs == 0:
            return save_path

        init = self._initialize(decay_steps = epoch_iters * decay_times, output_path = output_path, restore = restore,
                                prediction_path = prediciton_path)

        with tf.Session(config = util.gpu_config()) as sess:
            if write_graph:
                tf.train.write_graph(sess.graph_def, output_path, "graph.pb", False)

            sess.run(init)

            if restore:
                ckpt = tf.train.get_checkpoint_state(output_path)
                if ckpt and ckpt.model_checkpoint_path:
                    self.net.restore(sess, ckpt.model_checkpoint_path)

            if valid_provider is not None:
                valid_x, valid_y = valid_provider(self.verification_batch_size)
                pred_shape = self.store_prediction(sess, valid_x, valid_y, "_init")

            summary_writer = tf.summary.FileWriter(output_path, graph = sess.graph)
            logging.info("Start Optimization")

            avg_gradients = None
            for epoch in range(0, epochs):
                total_loss = 0
                for step in range(0, epoch_iters):
                    batch_x, batch_y = data_provider(self.batch_size)

                    _, loss, lr, gradients = sess.run(
                        (self.optimizer, self.net.cost, self.learning_rate_node, self.net.gradients_node),
                        feed_dict = {self.net.x: batch_x, self.net.y: util.crop_to_shape(batch_y, pred_shape),
                                     self.net.keep_prob: dropout})

                    if self.net.summaries and self.norm_grads:
                        avg_gradients = _update_avg_gradients(avg_gradients, gradients, step)
                        norm_gradients = [np.linalg.norm(gradient) for gradient in avg_gradients]
                        self.norm_gradients_node.assign(norm_gradients).eval()

                    if step % display_step == 0:
                        self.output_minibatch_stats(sess, summary_writer, step, batch_x,
                                                    util.crop_to_shape(batch_y, pred_shape))
                    total_loss += loss

                self.output_epoch_stats(epoch, total_loss, epoch_iters, lr)
                if valid_provider is not None:
                    self.store_prediction(sess, valid_x, valid_y, "epoch_%s" % epoch)

                self.net.save(sess, save_path, global_step = (self.global_step // epoch_iters))

            logging.info("Optimization Finished")

            return save_path

    def store_prediction(self, sess, batch_x, batch_y, name):
        prediction = sess.run(self.net.predicter,
                              feed_dict = {self.net.x: batch_x, self.net.y: batch_y, self.net.keep_prob: 1.})
        pred_shape = prediction.shape
        loss = sess.run(self.net.cost,
                        feed_dict = {self.net.x: batch_x, self.net.y: util.crop_to_shape(batch_y, pred_shape),
                                     self.net.keep_prob: 1.})
        logging.info("Verification error : {:.3f}%, loss : {:.7f}".format(
            error_rate(prediction, util.crop_to_shape(batch_y, prediction.shape)), loss))

        img = util.combine_img_prediction(batch_x, batch_y, prediction)
        util.save_image(img, "%s/%s.jpg" % (self.prediction_path, name))

        return pred_shape

    def output_epoch_stats(self, epoch, total_loss, epoch_iters, lr):
        logging.info(
            "Epoch {:}, Average loss : {:.7f}, learning rate : {:.4f}".format(epoch, (total_loss / epoch_iters), lr))

    def output_minibatch_stats(self, sess, summary_writer, step, batch_x, batch_y):
        summary_str, loss, acc, predictions = sess.run(
            [self.summary_op, self.net.cost, self.net.accuracy, self.net.predicter],
            feed_dict = {self.net.x: batch_x, self.net.y: batch_y, self.net.keep_prob: 1.})

        summary_writer.add_summary(summary_str, step)
        summary_writer.flush()
        logging.info(
            "Iter {:}, Minibatch loss = {:.7f}, Training Accuracy : {:.4f}, Minibatch error : {:.2f}%".format(step,
                                                                                                              loss, acc,
                                                                                                              error_rate(
                                                                                                                  predictions,
                                                                                                                  batch_y)))


def _update_avg_gradients(avg_gradients, gradients, step):
    if avg_gradients is None:
        avg_gradients = [np.zeros_like(gradient) for gradient in gradients]
    for i in range(len(gradients)):
        avg_gradients[i] = (avg_gradients[i] * (1.0 - (1.0 / (step + 1)))) + (gradients[i] / (step + 1))

    return avg_gradients


def error_rate(predictions, labels):
    return 100.0 - (100.0 * np.sum(np.argmax(predictions, 3) == np.argmax(labels, 3)) / (
            predictions.shape[0] * predictions.shape[1] * predictions.shape[2]))
